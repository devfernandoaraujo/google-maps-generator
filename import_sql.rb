#Import_sql.rb
module SQLImport extend self
    require 'pg'

    SQL_FILE = 'files/database_data.sql'

    def self.Import_sql()
        begin
            #Name of the new database
            new_database_name = 'googlenewmaps'

            #database connection parameters
            db_params = {
                host: 'localhost',
                port: 5432,
                dbname: 'postgres',
                user: 'postgres',
                password: 'postgres'
            }.freeze
            
            restoreDbCommand = "PGPASSWORD=#{db_params[:password]} psql -d #{new_database_name} -U #{db_params[:user]} -h #{db_params[:host]} -f #{SQL_FILE}"

            #Connect to database
            conn = PG.connect(db_params)
            
            # Check if the database exists
            result = conn.exec("SELECT 1 FROM pg_database WHERE datname = '#{new_database_name}';")
            puts result.num_tuples
            if result.num_tuples.zero?
                puts "The database '#{new_database_name}' does not exist."
            else
                puts "The database '#{new_database_name}' exists. Dropping it..."
                
                # Drop the database
                conn.exec("DROP DATABASE #{new_database_name};")
                puts "Database '#{new_database_name}' has been dropped."
            end

            # Create a new database
            conn.exec("CREATE DATABASE #{new_database_name}")
           
            # Restore the database
            result = system(restoreDbCommand)

            # Check the result of the command
            if result
                puts "pg_restore completed successfully."
            else
                puts "pg_restore failed."
            end

            rescue PG::Error => e
                puts "Error executing SQL file: #{e.message}"
            ensure
                conn.close if conn
        end
    end
end
